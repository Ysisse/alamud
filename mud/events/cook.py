from .event import Event3

class CookWithEvent(Event3):
    NAME = "cook"

    def perform(self):
        if not self.object.has_prop("cookable") or not self.object2.has_prop("cookable-with"):
            self.fail()
            return self.inform(self.NAME+".failed")
        self.inform(self.NAME)
