id: portal-depart-chambreFilette
type: Portal
exits:
  - id: portal-depart-chambreFilette-000
    location: zoneDepart
    direction: haut
    events:
      enter-portal:
        actor   : "La lumière s'allume."
        observer: "La lumière s'allume."
      leave-portal:
        observer: |
          {{ actor.name }} descend de l'arbre et vous rejoint
          sur le parking de l'IUT.
---
---
id: chambreMaFilette
type: Location
events:
  info:
    actor: ""
  look:
    actor: |
      Vous étes dans une chambre que vous avez jamais vu
      auparavant. A l'est il y a une porte, A l'ouest une armoire.
---
---
id: bouton-depart-000
type: Thing
name:
  - bouton
props:
  - pushable
events:
  look:
    actor: |
      Cela ressemble un un interrupteur pour allumer la lumière, vous pouvez APPUYER dessus.
  push:
    actor: |
      Vous appuyez sur l'interrupteur et la lumière s'allume.
    observer: |
      {{ actor.name }} appui sur l'interrupteur et la lumière s'allume.
    effects:
      - type  : EnterPortalEffect
        exit  : =portal-depart-chambreFilette
---
id: zoneDepart
type: Location
props:
  - dark
events:
  info: vous arrivez dans une pièce sombre vous REGARDER aux alentour pour essayer d'en apprendre plus.
    actor: ""
  look:
    actor: |
      Vous remarquez un interrupteur sur un mur à coté de vous pouvez peut-être REGARDER cet interrupteur de plus près.
parts:
  - bouton-depart-000
---